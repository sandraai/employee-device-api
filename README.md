# Employee Device API

This application serves a simple API for keeping track of employees and devices which are currently assigned to them.

## Installation & dependencies
Install Java JDK 17.0.2

Install Spring with Spring Boot DevTools and Spring Web

## Run application
Run the application by running src/main/java/com/scania/saib5g/employeedeviceapi/EmployeeDeviceApiApplication.java, then open localhost:8080/api/employees in your web browser
or use Postman with localhost:8080/api/employees to explore the API.

## How to use the API


### Get an employee
**Request:**
GET /api/employees/:id

**Response:**
Returns the employee with the specified id. HTTP/1.1 200 OK

If id does not exist in the repo, returns null. HTTP/1.1 404 Not Found
### Get all employees
**Request:**
GET /api/employees

**Response:**
Returns a map with all currently stored employees. HTTP/1.1 200 OK
### Create an employee
**Request:**
POST /api/employees, employee provided in request body

**Response:**
Returns the created employee. HTTP/1.1 201 Created

If a non-valid employee (null object, null/0 states, non-valid employee code, already existing employee) 
is provided, returns null. HTTP/1.1 400 Bad request
### Replace an employee
**Request:**
PUT /api/employees/:id, employee provided in request body

**Response:**
Returns the replaced employee. HTTP/1.1 200 OK

If employee does not exist, creates and returns created employee. HTTP/1.1 201 Created

If a non-valid employee (null object, null/0 states, non-valid employee code, already existing employee)
is provided, returns null. HTTP/1.1 400 Bad request
### Modify an employee
**Request:**
PATCH /api/employees/:id, employee provided in request body, must contain id state, all other states optional

**Response:**
If employee exists and input is valid (correct id provided), states provided in request are updated. HTTP/1.1 200 OK

If employee does not exist in repo, returns null. HTTP/1.1 404 Not Found

If id in path and request body does not match, or employee is null, returns null. HTTP/1.1 400 Bad request
### Delete an employee
**Request:**
DELETE /api/employees/:id

**Response:**
Returns confirmation string. HTTP/1.1 200 OK

If id does not exist in the repo, returns null. HTTP/1.1 404 Not Found
### Get all devices for an employee
**Request:**
GET /api/employees/:id/devices

**Response:**
Returns a map with all currently stored devices for employee with specified id. HTTP/1.1 200 OK
### Add a device for an employee
**Request:**
POST /api/employees/:id/devices, device provided in request body

**Response:**
Returns the created device. HTTP/1.1 201 Created

If a non-valid device (null object) is provided, returns null. HTTP/1.1 400 Bad request

### Replace a device for an employee
**Request:**
PUT /api/employees/:id/devices/:deviceId, device provided in request body

**Response:**
Returns the replaced device. HTTP/1.1 200 OK

If device does not exist, creates and returns created device. HTTP/1.1 201 Created

If a non-valid device (null object) is provided, returns null. HTTP/1.1 400 Bad request

### Delete a device for an employee
**Request:**
DELETE /api/employees/:id/devices/:deviceId

**Response:**
Returns confirmation string. HTTP/1.1 200 OK

If id does not exist in the employees device map, returns null. HTTP/1.1 404 Not Found

### Get all employees with their total number of devices
**Request**
GET /api/employees/devices/summary

**Response**
Returns a list with all employees and their total number of devices. HTTP/1.1 200 OK
### Get all recently hired employees
**Request**
GET /api/employees/new

**Response**
Returns a list with all employees hired the last two years. HTTP/1.1 200 OK
### Get all full-time employees and days remaining until their birthday
**Request**
GET /api/employees/birthdays

**Response**
Returns a list with all full-time employees, their birthdays and the number of days
remaining until their birthday. HTTP/1.1 200 OK


