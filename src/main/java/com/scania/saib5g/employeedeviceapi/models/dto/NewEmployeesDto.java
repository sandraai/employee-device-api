package com.scania.saib5g.employeedeviceapi.models.dto;

public class NewEmployeesDto {
    private String firstName;
    private String lastName;
    private long employeeCode;

    public NewEmployeesDto(String firstName, String lastName, long employeeCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeCode = employeeCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(long employeeCode) {
        this.employeeCode = employeeCode;
    }

}
