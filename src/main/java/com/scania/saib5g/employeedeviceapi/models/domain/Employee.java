package com.scania.saib5g.employeedeviceapi.models.domain;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Objects;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private EmploymentType employmentType;
    private Position position;
    private LocalDate dateHired;

    private long employeeCode;
    private HashMap<Integer, String> devices;

    public Employee(int id, String firstName, String lastName, LocalDate dateOfBirth, long employeeCode, LocalDate dateHired, Position position, EmploymentType employmentType, HashMap<Integer, String> devices) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.employmentType = employmentType;
        this.position = position;
        this.dateHired = dateHired;
        this.employeeCode = employeeCode;
        this.devices = devices;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public LocalDate getDateHired() {
        return dateHired;
    }

    public void setDateHired(LocalDate dateHired) {
        this.dateHired = dateHired;
    }

    public long getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(long employeeCode) {
        this.employeeCode = employeeCode;
    }

    public HashMap<Integer, String> getDevices() {
        return devices;
    }

    public void setDevices(HashMap<Integer, String> devices) {
        this.devices = devices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id && employeeCode == employee.employeeCode && Objects.equals(firstName, employee.firstName)
                && Objects.equals(lastName, employee.lastName) && Objects.equals(dateOfBirth, employee.dateOfBirth)
                && employmentType == employee.employmentType && position == employee.position
                && Objects.equals(dateHired, employee.dateHired) && Objects.equals(devices, employee.devices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, dateOfBirth, employmentType, position, dateHired, employeeCode, devices);
    }
}
