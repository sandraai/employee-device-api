package com.scania.saib5g.employeedeviceapi.models.dto;

import java.time.LocalDate;

public class FullTimeBirthdayDto {
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private long daysUntilNextBirthday;

    public FullTimeBirthdayDto(String firstName, String lastName, LocalDate dateOfBirth, long daysUntilNextBirthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.daysUntilNextBirthday = daysUntilNextBirthday;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getDaysUntilNextBirthday() {
        return daysUntilNextBirthday;
    }

    public void setDaysUntilNextBirthday(long daysUntilNextBirthday) {
        this.daysUntilNextBirthday = daysUntilNextBirthday;
    }

}
