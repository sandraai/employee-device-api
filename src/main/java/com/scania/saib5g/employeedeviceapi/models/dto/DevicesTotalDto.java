package com.scania.saib5g.employeedeviceapi.models.dto;

public class DevicesTotalDto {
    private String firstName;
    private String lastName;
    private int totalNumberOfDevices;

    public DevicesTotalDto(String firstName, String lastName, int totalNumberOfDevices) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalNumberOfDevices = totalNumberOfDevices;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTotalNumberOfDevices() {
        return totalNumberOfDevices;
    }

    public void setTotalNumberOfDevices(int totalNumberOfDevices) {
        this.totalNumberOfDevices = totalNumberOfDevices;
    }

}
