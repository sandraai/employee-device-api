package com.scania.saib5g.employeedeviceapi.models.maps;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.dto.NewEmployeesDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class NewEmployeesDtoMapper {

    public List<NewEmployeesDto> mapNewEmployees (Map<Integer, Employee> employees) {
        List<NewEmployeesDto> newEmployeesList = new ArrayList<>();
        LocalDate todaysDate = LocalDate.now();

        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            LocalDate dateHired = entry.getValue().getDateHired();
            if (todaysDate.minusYears(2).isBefore(dateHired)) {
                NewEmployeesDto newEmployeesDto = new NewEmployeesDto(entry.getValue().getFirstName(), entry.getValue().getLastName(), entry.getValue().getEmployeeCode());
                newEmployeesList.add(newEmployeesDto);
            }
        }
        return newEmployeesList;
    }

}
