package com.scania.saib5g.employeedeviceapi.models.domain;

public enum Position {
    DEVELOPER,
    TESTER,
    MANAGER
}
