package com.scania.saib5g.employeedeviceapi.models.domain;

public enum EmploymentType {
    FULL_TIME,
    PART_TIME
}
