package com.scania.saib5g.employeedeviceapi.models.domain;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LuhnCodeValidator {

    public boolean validateCode (long employeeCode) {
        String[] strings = String.valueOf(employeeCode).split("");
        if (strings.length != 10) {
            return false;
        }
        List<Integer> list = new ArrayList<>();
        int entry;

        for (int i = strings.length-1; i >= 0; i--) {
            entry = Integer.parseInt(strings[i]);
            if (i % 2 == 0) {
                entry = (entry * 2) % 10 + (entry * 2) / 10;
            }
            list.add(entry);
        }
        int sumOfDigits = list.stream()
                .reduce(0, Integer::sum);

        return sumOfDigits % 10 == 0;
    }
}
