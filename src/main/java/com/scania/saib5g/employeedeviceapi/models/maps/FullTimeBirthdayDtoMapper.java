package com.scania.saib5g.employeedeviceapi.models.maps;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.domain.EmploymentType;
import com.scania.saib5g.employeedeviceapi.models.dto.FullTimeBirthdayDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class FullTimeBirthdayDtoMapper {

    public List<FullTimeBirthdayDto> mapFullTimeEmployees (Map<Integer, Employee> employees) {
        List<FullTimeBirthdayDto> fullTimeEmployees = new ArrayList<>();
        LocalDate todaysDate = LocalDate.now();

        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            if (entry.getValue().getEmploymentType().equals(EmploymentType.FULL_TIME)) {

                LocalDate birthday = entry.getValue().getDateOfBirth();
                LocalDate nextBirthday = birthday.withYear(todaysDate.getYear());

                if (nextBirthday.isBefore(todaysDate)) {
                    nextBirthday = birthday.withYear(todaysDate.getYear()+1);
                }
                long daysUntilBirthday = ChronoUnit.DAYS.between(todaysDate, nextBirthday);

                FullTimeBirthdayDto fullTimeEmployeesBirthdayDto = new FullTimeBirthdayDto(entry.getValue().getFirstName(), entry.getValue().getLastName(), entry.getValue().getDateOfBirth(), daysUntilBirthday);
                fullTimeEmployees.add(fullTimeEmployeesBirthdayDto);
            }

        }
        return fullTimeEmployees;
    }
}
