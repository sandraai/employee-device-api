package com.scania.saib5g.employeedeviceapi.models.maps;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.dto.DevicesTotalDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DevicesTotalDtoMapper {

    public List<DevicesTotalDto> mapDevicesTotal (Map<Integer, Employee> employees) {
        List<DevicesTotalDto> totalDevicesList = new ArrayList<>();
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            DevicesTotalDto employeeDevicesTotalDto =
                    new DevicesTotalDto(entry.getValue().getFirstName(), entry.getValue().getLastName(), entry.getValue().getDevices().size());
            totalDevicesList.add(employeeDevicesTotalDto);
        }
        return totalDevicesList;
    }
}
