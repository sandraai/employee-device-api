package com.scania.saib5g.employeedeviceapi.controllers;

import com.scania.saib5g.employeedeviceapi.dataaccess.EmployeeRepository;
import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/employees")
public class EmployeeController {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController (EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping
    public ResponseEntity<Map<Integer, Employee>> getAllEmployees () {
        return new ResponseEntity<>(employeeRepository.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> getEmployee (@PathVariable int id) {
        if (employeeRepository.getEmployee(id) == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employeeRepository.getEmployee(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Employee> createEmployee (@RequestBody Employee employee) {
        if (employeeRepository.createEmployee(employee) == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(employeeRepository.createEmployee(employee), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> replaceEmployee (@PathVariable int id, @RequestBody Employee employee) {
        if (employeeRepository.replaceEmployee(employee) == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.employeeExistsInRepo(id)) {
            return new ResponseEntity<>(employeeRepository.createEmployee(employee), HttpStatus.CREATED);
        }

        return new ResponseEntity<>(employeeRepository.replaceEmployee(employee), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Employee> modifyEmployee (@PathVariable int id, @RequestBody Employee employee) {
        if (!employeeRepository.isMatchingIds(employee, id) || employeeRepository.modifyEmployee(employee) == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.employeeExistsInRepo(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employeeRepository.modifyEmployee(employee), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployee (@PathVariable int id) {
        if (employeeRepository.deleteEmployee(id) == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        employeeRepository.deleteEmployee(id);
        return new ResponseEntity<>("Employee was successfully deleted", HttpStatus.OK);
    }

    @GetMapping("/{id}/devices")
    public ResponseEntity<Map<Integer, String>> getAllDevices (@PathVariable int id) {
        return new ResponseEntity<>(employeeRepository.getAllDevices(id), HttpStatus.OK);
    }

    @PostMapping("/{id}/devices")
    public ResponseEntity<String> createDevice (@PathVariable int id, @RequestBody String device) {
        if (employeeRepository.addDevice(id, device) == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(employeeRepository.addDevice(id, device), HttpStatus.CREATED);
    }

    @PutMapping("/{id}/devices/{deviceId}")
    public ResponseEntity<String> replaceDevice (@PathVariable int id, @RequestBody String device, @PathVariable int deviceId) {
        if (employeeRepository.replaceDevice(id, device, deviceId) == null) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!employeeRepository.deviceExists(id, deviceId)) {
            return new ResponseEntity<>(employeeRepository.addDevice(id, device), HttpStatus.CREATED);
        }
        return new ResponseEntity<>(employeeRepository.replaceDevice(id, device, deviceId), HttpStatus.OK);
    }


    @DeleteMapping("/{id}/devices/{deviceId}")
    public ResponseEntity<String> deleteDevice (@PathVariable int id, @PathVariable int deviceId) {
        if (employeeRepository.deleteDevice(id, deviceId) == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        employeeRepository.deleteDevice(id, deviceId);
        return new ResponseEntity<>("Device was successfully deleted", HttpStatus.OK);
    }

    @GetMapping("/devices/summary")
    public ResponseEntity<List> totalDevices () {
        return new ResponseEntity<>(employeeRepository.getDevicesTotal(), HttpStatus.OK);
    }

    @GetMapping("/new")
    public ResponseEntity<List> newEmployees () {
        return new ResponseEntity<>(employeeRepository.getNewEmployees(), HttpStatus.OK);
    }

    @GetMapping("/birthdays")
    public ResponseEntity<List> fullTimeEmployeesBirthday () {
        return new ResponseEntity<>(employeeRepository.getFullTimeEmployees(), HttpStatus.OK);
    }
}
