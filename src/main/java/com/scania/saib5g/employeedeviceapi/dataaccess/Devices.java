package com.scania.saib5g.employeedeviceapi.dataaccess;

import java.util.Map;

public interface Devices {
    Map<Integer, String> getAllDevices(int id);
    String addDevice(int id, String device);
    String replaceDevice (int id, String device, int deviceId);
    String deleteDevice(int id, int deviceId);
    boolean deviceExists (int id, int deviceId);
}
