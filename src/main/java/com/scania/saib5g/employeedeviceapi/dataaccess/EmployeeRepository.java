package com.scania.saib5g.employeedeviceapi.dataaccess;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.domain.EmploymentType;
import com.scania.saib5g.employeedeviceapi.models.domain.LuhnCodeValidator;
import com.scania.saib5g.employeedeviceapi.models.domain.Position;
import com.scania.saib5g.employeedeviceapi.models.dto.DevicesTotalDto;
import com.scania.saib5g.employeedeviceapi.models.dto.FullTimeBirthdayDto;
import com.scania.saib5g.employeedeviceapi.models.dto.NewEmployeesDto;
import com.scania.saib5g.employeedeviceapi.models.maps.DevicesTotalDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.FullTimeBirthdayDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.NewEmployeesDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;


@Component
public class EmployeeRepository implements Repository, Devices {
    private Map<Integer, Employee> employees = seedEmployees();
    private LuhnCodeValidator luhnCodeValidator;
    private DevicesTotalDtoMapper devicesTotalDtoMapper;
    private NewEmployeesDtoMapper newEmployeesDtoMapper;
    private FullTimeBirthdayDtoMapper fullTimeBirthdayDtoMapper;

    @Autowired
    public EmployeeRepository(LuhnCodeValidator luhnCodeValidator, DevicesTotalDtoMapper devicesTotalDtoMapper, NewEmployeesDtoMapper newEmployeesDtoMapper, FullTimeBirthdayDtoMapper fullTimeBirthdayDtoMapper) {
        this.luhnCodeValidator = luhnCodeValidator;
        this.devicesTotalDtoMapper = devicesTotalDtoMapper;
        this.newEmployeesDtoMapper = newEmployeesDtoMapper;
        this.fullTimeBirthdayDtoMapper = fullTimeBirthdayDtoMapper;
    }


    private HashMap<Integer, Employee> seedEmployees () {
        var employees = new HashMap<Integer, Employee>();

        employees.put(1, new Employee(1, "John", "Doe", LocalDate.of(1980, 2, 14), 2018269734L,
                LocalDate.of(2021, 3, 31), Position.TESTER, EmploymentType.PART_TIME, new HashMap<>(Map.of(1, "Dell XPS 13"))));
        employees.put(2, new Employee(2, "Jane", "Doe", LocalDate.of(1984, 5, 17), 1634751505L,
                LocalDate.of(2018, 12, 1), Position.MANAGER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Alienware X17", 2, "Samsung 30-inch LED Monitor"))));
        employees.put(3, new Employee(3, "Sven", "Svensson", LocalDate.of(1987, 3, 22), 1507119608L,
                LocalDate.of(2019, 2, 15), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 15", 2, "Samsung 27-inch LED Monitor"))));

        return employees;
    }

    // CRUD methods for employees

    @Override
    public Employee getEmployee (int id) {
        return employees.get(id);
    }

    @Override
    public Map<Integer, Employee> getAllEmployees () {
        return employees;
    }

    @Override
    public Employee createEmployee (Employee employee) {
        if (!isValidEmployee(employee)) {
            return null;
        }
        if (employeeExistsInRepo(employee.getId())) {
            return null;
        }
        employees.put(employee.getId(), employee);
        return getEmployee(employee.getId());
    }

    @Override
    public Employee replaceEmployee (Employee employee) {
        if (!isValidEmployee(employee)) {
            return null;
        }
        if (!employeeExistsInRepo(employee.getId())) {
            return null;
        }
        Employee employeeToReplace = getEmployee(employee.getId());

        employees.remove(employeeToReplace.getId());
        employees.put(employee.getId(), employee);
        return getEmployee(employee.getId());
    }

    @Override
    public Employee modifyEmployee (Employee employee) {
        if (employee == null) {
            return null;
        }
        if (!employeeExistsInRepo(employee.getId())) {
            return null;
        }
        Employee employeeToModify = getEmployee(employee.getId());

        if (employee.getFirstName() != null) {
            employeeToModify.setFirstName(employee.getFirstName());
        }
        if (employee.getLastName() != null) {
            employeeToModify.setLastName(employee.getLastName());
        }
        if (employee.getDateOfBirth() != null) {
            employeeToModify.setDateOfBirth(employee.getDateOfBirth());
        }
        if (isValidEmployeeCode(employee.getEmployeeCode())) {
            employeeToModify.setEmployeeCode(employee.getEmployeeCode());
        }
        if (employee.getDateHired() != null) {
            employeeToModify.setDateHired(employee.getDateHired());
        }
        if (employee.getPosition() != null) {
            employeeToModify.setPosition(employee.getPosition());
        }
        if (employee.getEmploymentType() != null) {
            employeeToModify.setEmploymentType(employee.getEmploymentType());
        }
        if (employee.getDevices() != null) {
            employeeToModify.setDevices(employee.getDevices());
        }
        return getEmployee(employee.getId());
    }

    @Override
    public Employee deleteEmployee (int id) {
        return employees.remove(id);
    }

    // validation methods for employees

    @Override
    public boolean isValidEmployeeCode (long employeeCode) {
        return luhnCodeValidator.validateCode(employeeCode);
    }

    @Override
    public boolean isValidEmployee (Employee employee) {
        if (employee == null) {
            return false;
        }
        if (!isValidEmployeeCode(employee.getEmployeeCode()) || employeeCodeExistsInRepo(employee.getEmployeeCode())) {
            return false;
        }
        return employee.getId() > 0 && employee.getFirstName() != null && employee.getLastName() != null
                && employee.getDateOfBirth() != null && employee.getDateHired() != null
                && employee.getPosition() != null && employee.getEmploymentType() != null
                && employee.getDevices() != null;
    }

    @Override
    public boolean isMatchingIds (Employee employee, int id) {
        return employee.getId() == id;
    }

    @Override
    public boolean employeeExistsInRepo (int id) {
        return employees.containsKey(id);
    }

    @Override
    public boolean employeeCodeExistsInRepo (long employeeCode) {
        for (Map.Entry<Integer, Employee> entry : employees.entrySet()) {
            if (entry.getValue().getEmployeeCode() == employeeCode) {
                return true;
            }
        }
        return false;
    }

    // CRUD methods and validation for devices

    @Override
    public Map<Integer, String> getAllDevices(int id) {
        return getEmployee(id).getDevices();
    }

    @Override
    public String addDevice(int id, String device) {
        if (device == null) {
            return null;
        }
        Map<Integer, String> employeeDevices = getEmployee(id).getDevices();
        int key = getEmployee(id).getDevices().size() + 1;
        employeeDevices.put(key, device);
        return getEmployee(id).getDevices().get(key);
    }

    @Override
    public String replaceDevice (int id, String device, int deviceId) {
        if (device == null) {
            return null;
        }
        if (!deviceExists(id, deviceId)) {
            return null;
        }
        Map<Integer, String> employeeDevices = getEmployee(id).getDevices();
        employeeDevices.remove(deviceId);
        employeeDevices.put(deviceId, device);
        return getEmployee(id).getDevices().get(deviceId);
    }

    @Override
    public String deleteDevice(int id, int deviceId) {
        return getEmployee(id).getDevices().remove(deviceId);
    }

    @Override
    public boolean deviceExists (int id, int deviceId) {
        Map<Integer, String> employeeDevices = getEmployee(id).getDevices();
        return employeeDevices.containsKey(deviceId);
    }

    // DTO methods

    @Override
    public List<DevicesTotalDto> getDevicesTotal () {
        return devicesTotalDtoMapper.mapDevicesTotal(employees);
    }

    @Override
    public List<NewEmployeesDto> getNewEmployees () {
        return newEmployeesDtoMapper.mapNewEmployees(employees);
    }

    @Override
    public List<FullTimeBirthdayDto> getFullTimeEmployees () {
        return fullTimeBirthdayDtoMapper.mapFullTimeEmployees(employees);
    }

}

