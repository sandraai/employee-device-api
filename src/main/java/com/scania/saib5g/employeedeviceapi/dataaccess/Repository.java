package com.scania.saib5g.employeedeviceapi.dataaccess;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.dto.DevicesTotalDto;
import com.scania.saib5g.employeedeviceapi.models.dto.FullTimeBirthdayDto;
import com.scania.saib5g.employeedeviceapi.models.dto.NewEmployeesDto;

import java.util.List;
import java.util.Map;

public interface Repository {
    // CRUD methods
    Employee getEmployee (int id);
    Map<Integer, Employee> getAllEmployees ();
    Employee createEmployee (Employee employee);
    Employee replaceEmployee (Employee employee);
    Employee modifyEmployee (Employee employee);
    Employee deleteEmployee (int id);

    // validation methods
    boolean isMatchingIds (Employee employee, int id);
    boolean employeeExistsInRepo (int id);
    boolean isValidEmployeeCode (long employeeCode);
    boolean isValidEmployee (Employee employee);
    boolean employeeCodeExistsInRepo (long employeeCode);

    // DTO methods
    List<DevicesTotalDto> getDevicesTotal ();
    List<NewEmployeesDto> getNewEmployees ();
    List<FullTimeBirthdayDto> getFullTimeEmployees ();
}
