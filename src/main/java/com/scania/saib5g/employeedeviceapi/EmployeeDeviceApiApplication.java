package com.scania.saib5g.employeedeviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeDeviceApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeDeviceApiApplication.class, args);
    }

}
