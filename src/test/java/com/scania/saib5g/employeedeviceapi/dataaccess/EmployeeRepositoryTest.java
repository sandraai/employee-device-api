package com.scania.saib5g.employeedeviceapi.dataaccess;

import com.scania.saib5g.employeedeviceapi.models.domain.Employee;
import com.scania.saib5g.employeedeviceapi.models.domain.EmploymentType;
import com.scania.saib5g.employeedeviceapi.models.domain.LuhnCodeValidator;
import com.scania.saib5g.employeedeviceapi.models.domain.Position;
import com.scania.saib5g.employeedeviceapi.models.maps.DevicesTotalDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.FullTimeBirthdayDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.NewEmployeesDtoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeRepositoryTest {
    private EmployeeRepository employeeRepository;
    private DevicesTotalDtoMapper devicesTotalDtoMapper;
    private NewEmployeesDtoMapper newEmployeesDtoMapper;
    private FullTimeBirthdayDtoMapper fullTimeBirthdayDtoMapper;
    private LuhnCodeValidator luhnCodeValidator;

    @BeforeEach
    void initRepository() {
        devicesTotalDtoMapper = new DevicesTotalDtoMapper();
        newEmployeesDtoMapper = new NewEmployeesDtoMapper();
        fullTimeBirthdayDtoMapper = new FullTimeBirthdayDtoMapper();
        luhnCodeValidator = new LuhnCodeValidator();
        employeeRepository = new EmployeeRepository(luhnCodeValidator, devicesTotalDtoMapper, newEmployeesDtoMapper, fullTimeBirthdayDtoMapper);
    }

    // testing getEmployee
    @Test
    void getEmployee_existingId_existingEmployee() {
        int testNumber = 1;
        Employee expected = new Employee(1, "John", "Doe", LocalDate.of(1980, 2, 14), 2018269734L,
                LocalDate.of(2021, 3, 31), Position.TESTER, EmploymentType.PART_TIME, new HashMap<>(Map.of(1, "Dell XPS 13")));

        Employee actual = employeeRepository.getEmployee(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void getEmployee_nonExistingId_returnsNull() {
        int testNumber = 4;
        Employee expected = null;

        Employee actual = employeeRepository.getEmployee(testNumber);

        assertEquals(expected, actual);
    }

    // testing getAllEmployees
    @Test
    void getAllEmployees_returnsThree() {
        int expected = 3;

        int actual = employeeRepository.getAllEmployees().size();

        assertEquals(expected, actual);
    }

    // testing createEmployee
    @Test
    void createEmployee_validEmployee_ReturnsEmployee() {
        Employee expected = new Employee(4, "Johan", "Johansson", LocalDate.of(1989, 10, 13), 3680651936L,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        Employee actual = employeeRepository.createEmployee(expected);

        assertEquals(expected, actual);
    }

    @Test
    void createEmployee_notValidEmployeeIsNull_ReturnsNull() {
        Employee testEmployee = null;

        Employee expected = null;

        Employee actual = employeeRepository.createEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void createEmployee_existingEmployeeId_ReturnsNull() {
        Employee testEmployee = new Employee(1, "John", "Doe", LocalDate.of(1980, 2, 14), 2018269734L,
                LocalDate.of(2021, 3, 31), Position.TESTER, EmploymentType.PART_TIME, new HashMap<>(Map.of(1, "Dell XPS 13")));

        Employee expected = null;

        Employee actual = employeeRepository.createEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    // testing replaceEmployee
    @Test
    void replaceEmployee_validEmployee_ReturnsEmployee() {
        Employee expected = new Employee(3, "Johan", "Johansson", LocalDate.of(1989, 10, 13), 3680651936L,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        Employee actual = employeeRepository.replaceEmployee(expected);

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_notValidEmployeeIsNull_ReturnsNull() {
        Employee testEmployee = null;

        Employee expected = null;

        Employee actual = employeeRepository.replaceEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void replaceEmployee_nonExistingEmployeeId_ReturnsNull() {
        Employee testEmployee = new Employee(4, "Johan", "Johansson", LocalDate.of(1989, 10, 13), 3680651936L,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        Employee expected = null;

        Employee actual = employeeRepository.replaceEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    // testing modifyEmployee
    @Test
    void modifyEmployee_validEmployee_ReturnsEmployee() {
        Employee testEmployee = new Employee(3, null, "Johansson", null, 0, null,
                Position.TESTER, EmploymentType.PART_TIME, null);

        Employee expected = new Employee(3, "Sven", "Johansson", LocalDate.of(1987, 3, 22), 1507119608L,
                LocalDate.of(2019, 2, 15), Position.TESTER, EmploymentType.PART_TIME, new HashMap<>(Map.of(1, "Dell XPS 15", 2, "Samsung 27-inch LED Monitor")));

        Employee actual = employeeRepository.modifyEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void modifyEmployee_notValidEmployeeIsNull_ReturnsNull() {
        Employee testEmployee = null;

        Employee expected = null;

        Employee actual = employeeRepository.modifyEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void modifyEmployee_nonExistingEmployeeId_ReturnsNull() {
        Employee testEmployee = new Employee(4, "Johan", "Johansson", LocalDate.of(1989, 10, 13), 3680651936L,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        Employee expected = null;

        Employee actual = employeeRepository.modifyEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    // testing deleteEmployee
    @Test
    void deleteEmployee_existingId_existingEmployee() {
        int testNumber = 1;
        Employee expected = new Employee(1, "John", "Doe", LocalDate.of(1980, 2, 14), 2018269734L,
                LocalDate.of(2021, 3, 31), Position.TESTER, EmploymentType.PART_TIME, new HashMap<>(Map.of(1, "Dell XPS 13")));

        Employee actual = employeeRepository.deleteEmployee(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void deleteEmployee_nonExistingId_returnsNull() {
        int testNumber = 4;
        Employee expected = null;

        Employee actual = employeeRepository.deleteEmployee(testNumber);

        assertEquals(expected, actual);
    }

    // testing isValidEmployee
    @Test
    void isValidEmployee_employeeCodeAlreadyExists_returnsNull() {
        Employee testEmployee = new Employee(4, "Johan", "Johansson", LocalDate.of(1989, 10, 13), 2018269734,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        boolean expected = false;
        boolean actual = employeeRepository.isValidEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    @Test
    void isValidEmployee_firstNameIsNull_returnsNull() {
        Employee testEmployee = new Employee(4, null, "Johansson", LocalDate.of(1989, 10, 13), 3680651936L,
                LocalDate.of(2020, 8, 22), Position.DEVELOPER, EmploymentType.FULL_TIME, new HashMap<>(Map.of(1, "Dell XPS 13", 2, "Samsung 30-inch LED Monitor")));

        boolean expected = false;
        boolean actual = employeeRepository.isValidEmployee(testEmployee);

        assertEquals(expected, actual);
    }

    // testing getAllDevices
    @Test
    void getAllDevices_employeeIdTwo_returnsTwo() {
        int testId = 2;
        int expected = 2;

        int actual = employeeRepository.getAllDevices(testId).size();

        assertEquals(expected, actual);
    }

    // testing addDevice
    @Test
    void addDevice_validDevice_ReturnsDevice() {
        int testId = 2;
        String expected = "Keyboard";

        String actual = employeeRepository.addDevice(testId, expected);

        assertEquals(expected, actual);
    }

    @Test
    void addDevice_notValidDeviceIsNull_ReturnsNull() {
        int testId = 2;

        String expected = null;

        String actual = employeeRepository.addDevice(testId, expected);

        assertEquals(expected, actual);
    }

    // testing replaceDevice
    @Test
    void replaceDevice_validDevice_ReturnsDevice() {
        int testId = 2;
        int testDeviceId = 1;
        String expected = "Keyboard";

        String actual = employeeRepository.replaceDevice(testId, expected, testDeviceId);

        assertEquals(expected, actual);
    }

    @Test
    void replaceDevice_notValidDeviceIsNull_ReturnsNull() {
        int testId = 2;
        int testDeviceId = 1;

        String expected = null;

        String actual = employeeRepository.replaceDevice(testId, expected, testDeviceId);

        assertEquals(expected, actual);
    }

    @Test
    void replaceDevice_nonExistingDeviceId_ReturnsNull() {
        int testId = 2;
        int testDeviceId = 3;
        String testDevice = "Keyboard";

        String expected = null;

        String actual = employeeRepository.replaceDevice(testId, testDevice, testDeviceId);

        assertEquals(expected, actual);
    }

    // testing deleteDevice
    @Test
    void deleteDevice_existingId_existingDevice() {
        int testId = 2;
        int testDeviceId = 1;
        String expected = "Alienware X17";

        String actual = employeeRepository.deleteDevice(testId, testDeviceId);

        assertEquals(expected, actual);
    }

    @Test
    void deleteDevice_nonExistingId_returnsNull() {
        int testId = 2;
        int testDeviceId = 3;
        String expected = null;

        String actual = employeeRepository.deleteDevice(testId, testDeviceId);

        assertEquals(expected, actual);
    }
}