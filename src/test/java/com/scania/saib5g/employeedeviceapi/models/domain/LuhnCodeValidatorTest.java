package com.scania.saib5g.employeedeviceapi.models.domain;

import com.scania.saib5g.employeedeviceapi.dataaccess.EmployeeRepository;
import com.scania.saib5g.employeedeviceapi.models.maps.DevicesTotalDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.FullTimeBirthdayDtoMapper;
import com.scania.saib5g.employeedeviceapi.models.maps.NewEmployeesDtoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnCodeValidatorTest {
    private LuhnCodeValidator luhnCodeValidator;

    @BeforeEach
    void initRepository () {
        luhnCodeValidator = new LuhnCodeValidator();
    }

    @Test
    void isValidEmployeeCode_validEmployeeCode_ReturnsTrue () {
        long testNumber = 1054765944L;
        boolean expected = true;

        boolean actual = luhnCodeValidator.validateCode(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void isValidEmployeeCode_10DigitNotValidLuhnNumber_ReturnsFalse () {
        long testNumber = 5268320415L;
        boolean expected = false;

        boolean actual = luhnCodeValidator.validateCode(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void isValidEmployeeCode_8DigitValidLuhnNumber_ReturnsFalse () {
        long testNumber = 79526612L;
        boolean expected = false;

        boolean actual = luhnCodeValidator.validateCode(testNumber);

        assertEquals(expected, actual);
    }

    @Test
    void isValidEmployeeCode_12DigitValidLuhnNumber_ReturnsFalse () {
        long testNumber = 275077065678L;
        boolean expected = false;

        boolean actual = luhnCodeValidator.validateCode(testNumber);

        assertEquals(expected, actual);
    }
}