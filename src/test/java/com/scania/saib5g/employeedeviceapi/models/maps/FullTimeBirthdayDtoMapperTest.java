package com.scania.saib5g.employeedeviceapi.models.maps;

import com.scania.saib5g.employeedeviceapi.dataaccess.EmployeeRepository;
import com.scania.saib5g.employeedeviceapi.models.domain.LuhnCodeValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FullTimeBirthdayDtoMapperTest {
    private EmployeeRepository employeeRepository;
    private DevicesTotalDtoMapper devicesTotalDtoMapper;
    private NewEmployeesDtoMapper newEmployeesDtoMapper;
    private FullTimeBirthdayDtoMapper fullTimeBirthdayDtoMapper;
    private LuhnCodeValidator luhnCodeValidator;

    @BeforeEach
    void initRepository () {
        devicesTotalDtoMapper = new DevicesTotalDtoMapper();
        newEmployeesDtoMapper = new NewEmployeesDtoMapper();
        fullTimeBirthdayDtoMapper = new FullTimeBirthdayDtoMapper();
        luhnCodeValidator = new LuhnCodeValidator();
        employeeRepository = new EmployeeRepository(luhnCodeValidator, devicesTotalDtoMapper, newEmployeesDtoMapper, fullTimeBirthdayDtoMapper);
    }

    @Test
    void mapFullTimeEmployees_returnsListSizeOfTwo () {
        int expected = 2;
        int actual = fullTimeBirthdayDtoMapper.mapFullTimeEmployees(employeeRepository.getAllEmployees()).size();

        assertEquals(expected, actual);
    }
}